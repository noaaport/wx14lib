#include <stdio.h>
/*
#include <unistd.h>
#include <fcntl.h>
*/
#include <strings.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <err.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include "wx14.h"

static int save_data(void *buf, size_t size){
  
  char file[2];
  int fd;
  static int i = 0;

  file[0] = 'a' + i;
  file[1] = '\0';
  fd = open(file, O_RDWR|O_CREAT, 0644);
  if(fd == -1)
    err(1, "open");

  if(write(fd, buf, size) == -1)
    err(1, "write");

  close(fd);

  ++i;

  return(0);
}

static int save_status(struct wx14_msg_st *wx14msg){

  char *statusfile = "wx14.status";
  int status = 0;

  fprintf(stdout, "%ju %ju\n",
	  wx14msg->wx14ss.unixseconds,
	  wx14msg->wx14ss.unixseconds_lastlog);

  if(wx14msg->wx14ss.unixseconds != wx14msg->wx14ss.unixseconds_lastlog)
    status = wx14_signalstatus_log(statusfile, wx14msg);

  return(status);
}

int main(void){

  /*
  char *ip = "wx14.fincher.net";
  char *port = "7002";
  */
  char *ip = "67.216.244.152";
  int port = 7002;
  int sfd;
  struct sockaddr_in serveraddr;
  struct wx14_msg_st wx14msg;
  int status = 0;

  sfd = socket(AF_INET, SOCK_STREAM, 0);
  if(sfd == -1)
    return(1);

  bzero(&serveraddr, sizeof(serveraddr));
  serveraddr.sin_family = AF_INET;
  serveraddr.sin_port = htons(port);
  inet_pton(AF_INET, ip, &serveraddr.sin_addr);

  if(connect(sfd, (struct sockaddr *)&serveraddr, sizeof(serveraddr)) != 0){
    err(1, "connect");
    return(1);
  }

  wx14_init(&wx14msg);
  status = wx14_init_emwin_block(sfd, 1, 2, &wx14msg);
  if(status == -1)
    err(1, "Error");
  else if(status != 0)
    errx(1, "Error %d", status);

  while(status == 0){
    fprintf(stdout, "%d\n", 1);
    status = wx14_read_emwin_block(sfd, 1, 2, &wx14msg);
    fprintf(stdout, "%d\n", 2);

    if(status == 0){

      fprintf(stdout, "%d\n", wx14msg.emwin_block_size);

      fprintf(stdout, "%d\n", 3);
      /*save_data(wx14msg.emwin_block, wx14msg.emwin_block_size); */
      save_status(&wx14msg);
      fprintf(stdout, "%d\n", 4);

      /*
      fprintf(stdout, "%u\n", (unsigned int)size);
      save_data(data, size);
      */

    } else
      fprintf(stdout, "Error: %d\n", status);
  }

  return(0);
}
