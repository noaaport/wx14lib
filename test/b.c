#include <stdio.h>
/*
#include <unistd.h>
#include <fcntl.h>
*/
#include <strings.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <err.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include "wx14.h"
#include "wx14_private.h"

static int save_data(void *buf, size_t size){
  
  char file[2];
  int fd;
  static int i = 0;

  file[0] = 'a' + i;
  file[1] = '\0';
  fd = open(file, O_RDWR|O_CREAT, 0644);
  if(fd == -1)
    err(1, "open");

  if(write(fd, buf, size) == -1)
    err(1, "write");

  close(fd);

  ++i;

  return(0);
}

static int save_status(struct wx14_msg_st *wx14msg){

  char *statusfile = "wx14.status";
  int status = 0;

  fprintf(stdout, "%ju %ju\n",
	  wx14msg->wx14ss.unixseconds,
	  wx14msg->wx14ss.unixseconds_lastlog);

  if(wx14msg->wx14ss.unixseconds != wx14msg->wx14ss.unixseconds_lastlog)
    status = wx14_signalstatus_log(statusfile, wx14msg);

  return(status);
}

int main(void){

  /*
  char *ip = "wx14.fincher.net";
  char *port = "7002";
  */
  char *ip = "67.216.244.152";
  int port = 7003;
  int sfd;
  struct sockaddr_in serveraddr;
  struct wx14_msg_st wx14msg;
  unsigned char data[1116];
  int SIZE = 1116;
  size_t size = 1116;
  int type = 0;
  int status = 0;
  int n;

  sfd = socket(AF_INET, SOCK_STREAM, 0);
  if(sfd == -1)
    return(1);

  bzero(&serveraddr, sizeof(serveraddr));
  serveraddr.sin_family = AF_INET;
  serveraddr.sin_port = htons(port);
  inet_pton(AF_INET, ip, &serveraddr.sin_addr);

  if(connect(sfd, (struct sockaddr *)&serveraddr, sizeof(serveraddr)) != 0){
    err(1, "connect");
    return(1);
  }

  while(status == 0){

    size = SIZE;
    n = readn(sfd, data, size, 1, 2);
    if(n != (int)size){
      errx(1, "readn[%d]", n);
      status = 1;
    }
    size = n;

    write(1, data, n);
  }

  return(0);
}
