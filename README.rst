Overview
========

This is a standalone version of the library that is used by
Npemwin to interface the WX14 device. This library can be used by
other applications, for example to monitor the signal status and
configuring the device.
